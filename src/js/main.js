//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js
//= ../../bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js
//= ../../bower_components/jquery-mousewheel/jquery.mousewheel.min.js
//= ../../bower_components/Swiper/dist/js/swiper.js

window.app = {
  global: {
    initImgResizer: function() {
       /* var img = $(".event-item");
        $(".event-item").outerHeight(img.outerWidth());*/
        $(window).resize(function(){
          var img = $(".event-item");
          $(".event-item").outerHeight(img.outerWidth());
          if ($(".event-item").outerWidth() < 380) {
            var img = $("._mobile");
            $("._mobile").outerHeight(img.outerWidth());
          }
        });
        $(window).resize();
    },
    initCustomScroll: function() {
      (function($){
        $(window).load(function(){
            $(".aside").mCustomScrollbar({
              theme:"minimal-dark",
              mouseWheelPixels: 300
            });
            $("body").mCustomScrollbar({
              theme:"minimal-dark",
              mouseWheelPixels: 400
            });
        });
    })(jQuery);
    },
    init: function() {
      this.initImgResizer();
      this.initCustomScroll();
    }
  },
  main: {
    initMainSlider: function() {
      var mySwiper = new Swiper ('.main-slider .swiper-container', {
        loop: true,
        slidesPerView: 4,
        spaceBetween: 5,
        centeredSlides: true,
        nextButton: $('.main-slider .swiper-button-next')[0],
        prevButton: $('.main-slider .swiper-button-prev')[0]
      })
    },
    init: function() {
      this.initMainSlider()
    }
  }
}
